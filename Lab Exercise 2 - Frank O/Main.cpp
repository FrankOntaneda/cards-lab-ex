
// Frank Ontaneda
// Lab Exercise 2 - Playing Cards

#include <iostream>
#include <conio.h>
#include <string>

using namespace std;

enum Rank // Ace is high (14)
{
	Two = 2, Three, Four, Five, Six, Seven, Eight, Nine, Ten, Jack, Queen, King, Ace
};

enum Suit
{
	Hearts, Clubs, Diamonds, Spades
};

struct Card
{
	Rank rank;
	Suit suit;
};

int main()
{
	Card card1;
	card1.rank = Five;
	card1.suit = Spades;

	//cout << "The " + card1.rank + card1.suit;

	_getch();
	return 0;
}
